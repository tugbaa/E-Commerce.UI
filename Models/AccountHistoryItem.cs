﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Commerce.UI.Models
{
    public class AccountHistoryItem
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
