﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Security;

namespace E_Commerce.UI.Models
{
    public class AccountModels 
    {
       /* public AccountModels()
        {
            AccountHistory=new List<AccountHistoryItem>
        }
        */
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }

        public List<AccountHistoryItem> AccountHistory { get; set; }
    }
}
