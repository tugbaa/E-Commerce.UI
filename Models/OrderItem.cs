﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E_Commerce.UI.Models
{
    public class OrderItem
    {
        public int OrderItemId { get; set; }
        public int ProductId { get; set; }

        public string Description { get; set; }

        public string ImageUrl { get; set; }

        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
}
