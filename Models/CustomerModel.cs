﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E_Commerce.UI.Models
{
    public class CustomerModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }

        public string Address1 { get; set; }
        public string Town { get; set; }
        public string PostCode { get; set; }
    }
}